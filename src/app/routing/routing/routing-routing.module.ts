import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IncomeComponentComponent} from '../../income-component/income-component.component'; 
import { OutcomeComponentComponent} from '../../outcome-component/outcome-component.component';
import { Err404Component} from '../../err404/err404.component';
// import { CommonModule } from '@angular/common';

const routes: Routes = [
	{'path':'income',component:IncomeComponentComponent},
	{'path':'outcome',component:OutcomeComponentComponent},
	{'path':'**',component:Err404Component},
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class RoutingRoutingModule { }
export const routeComponents=[IncomeComponentComponent]
