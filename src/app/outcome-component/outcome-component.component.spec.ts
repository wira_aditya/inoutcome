import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutcomeComponentComponent } from './outcome-component.component';

describe('OutcomeComponentComponent', () => {
  let component: OutcomeComponentComponent;
  let fixture: ComponentFixture<OutcomeComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutcomeComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutcomeComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
