import { Component, OnInit } from '@angular/core';
import {IncomeI} from './income-i';
import {Income} from './income';
import {IncomeserviceService} from './service/incomeservice.service';

// 
declare var $:any;
@Component({
  selector: 'app-income-component',
  templateUrl: './income-component.component.html',
  styleUrls: ['./income-component.component.css']
})
export class IncomeComponentComponent implements OnInit {
  constructor(private service:IncomeserviceService) { }
  public incomeData:any = [];
  forms = new Income;
  private titlle="";
  getData(){
    this.service.getDataIncome().then(
      data=>{
        console.log(data[0].nama)
        this.incomeData=data
      }
    )
  }
  editIncome(idIncome){
    // find buat nyari 1
    // filter buat nyari lebih
    let dataEdit = this.incomeData.find(income=>income.idIn == idIncome);
    this.forms.setValue(dataEdit.idIn,dataEdit.nama,dataEdit.jenis,dataEdit.jumlah,dataEdit.date);
    console.log(this.forms);
  }
  submitIncome(){
    console.log(this.forms.idIn);
    this.service.submitDataIncome(this.forms).then(
      res=>{
        console.log(res);
        if(res['success']) {
          $('#form-modal').modal('hide')
          this.getData();
        }else{
          alert(res['msg']);
        }
      }
     )
  }
  deleteIncome(idIncome){
    if(confirm("Hapus data ini ?")) {
      this.service.deleteDataIncome(idIncome).then(
        res=>{
          this.getData()
        }
      )
    }
  }
  ngOnInit() {
  	this.getData()
  }

}
