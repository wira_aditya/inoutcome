import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Income} from '../income';
import {IncomeI} from '../income-i';
import {Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class IncomeserviceService {
  api="http://localhost/inoutcome/api/income"
  constructor(private http:HttpClient) { }
  // promis buat gaperlu pake observable fetch data pake then bukan subsribe atau map
  getDataIncome(){
  	return this.http.get(this.api+'/readIncome.php').toPromise();
  }
  submitDataIncome(data){
  	return this.http.post(this.api+'/submitIncome.php',data).toPromise();
  }
  deleteDataIncome(idIncome){
    return this.http.delete(this.api+'/deleteIncome.php',{params:{id:idIncome}}).toPromise();
  }
}
