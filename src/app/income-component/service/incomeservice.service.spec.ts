import { TestBed, inject } from '@angular/core/testing';

import { IncomeserviceService } from './incomeservice.service';

describe('IncomeserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IncomeserviceService]
    });
  });

  it('should be created', inject([IncomeserviceService], (service: IncomeserviceService) => {
    expect(service).toBeTruthy();
  }));
});
