import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'
import {RoutingRoutingModule} from './routing/routing/routing-routing.module';
import {HttpClientModule} from '@angular/common/http'

import { AppComponent } from './app.component';
import { IncomeComponentComponent } from './income-component/income-component.component';
import { OutcomeComponentComponent } from './outcome-component/outcome-component.component';
import { Err404Component } from './err404/err404.component';

@NgModule({
  declarations: [
    AppComponent,
    IncomeComponentComponent,
    OutcomeComponentComponent,
    Err404Component,
  ],
  imports: [
    RoutingRoutingModule,
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
