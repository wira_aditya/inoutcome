<?php  
	include_once "../connection.php";
	$q = $con->query("SELECT * FROM income ");
	$data = array();
	if ($q->num_rows) {
		$key=0;
		while ($row = $q->fetch_assoc()) {
			$data[] = $row;
			$data[$key]['dateTxt'] = date("d M Y",strtotime($row['date']));
			$key++;
		}
	}
	print_r(json_encode($data));
?>